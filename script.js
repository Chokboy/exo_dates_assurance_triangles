document.onload = function() {
}
window.onload = function() {

}

// sert à lancer une fonction une fois la page ou fenetre ayant déjà fini de charger / se dessiner
// (pour que la fonction reste active même si la page a fini de tout charger)

// _______________________BONUS MSG_____________________________________________________________

/**
 * on écoute un évennement -> au clic de la souris, on lance une fonction
 * cette fonction cible dans le doc un élément par son id, 
 * et (append)-> rajoute du contenu au noeud parent.
 */
document.getElementById("btn", function(){

    addmsg();
    
});

function addmsg() {
  
  document.getElementById("btn").append(" + un nouveau");
}
// _______________________BONUS MSG______END_______________________________________________________

// _________________________CHANGE BCKG COLOR___________________________________________________________

/**
 * ici on écoute un évenemment -> clicl de la souris, pour lancer une fonction :
 * cette fonction cible le document entier, la balise body,
 * et joue avec son style -> ici le background (color)
 */
document.addEventListener("click", function(){
    
    document.body.style.backgroundColor = "red";
});

// _________________________CHANGE BCKG COLOR_________END_______________________________________________


// __________________________CHANGE POSITION__________________________________________________________
/**
 * ici on attend que la fenetre se charge en premier lieu pour ensuite pouvoir éxécuter notre fonction
 * on déclare une variable test à laquelle on assimile le fait qu'elle cible un élément par son id.
 * on utilise donc la variable test qui att comme info -> survol de la souris, pour lancer la fonction :
 * 
 */

    let test = document.getElementById("pos");

    test.addEventListener("mouseover", function () {
        pos();
    });
    
    
     
      function pos() {
        test.style.marginLeft = "1000px";
      }

// __________________________CHANGE POSITION_________END______________________________________________

// __________________________ALERT BOX DBL CLICK________________________________________________________


    let buttontest = document.getElementById("btn");

    buttontest.addEventListener("dblclick", function () {
        msg();
    });


    
    function msg() {
        alert("PAS SI VITE PUTAIN");
    }

// __________________________ALERT BOX DBL CLICK_______END________________________________________________

// ______________________________________________________________________________________

// si 5 clics -> afficher un truc ? 

// var compteurval = 0;

// function incrementation() {
//     updateDisplay(++compteurval);
// }

// function compteur() {
//     document.getElementById("compteur").innerHTML = val;
// }

// let count = getElementById("compteur");

// count.addEventListener("click", function () {
//     incrementation();
// });


// _____________________________________DRAG N DROP_______THIERRY______________________________________
// window.onload = function(event){ 

//   let objectOnMove;

//   document.addEventListener("dragstart", function(event){
//       // Il faut définir notre élément à bouger en sélectionnant la cible qui subit l'évènement au début du drag
//       objectOnMove = event.target;
//   })

//   document.addEventListener("dragover", function(event){
//       // Force à autorisé le déplacement
//       event.preventDefault();
//   })

//   document.addEventListener("drop", function(event){

//       // Lorsque sa position change il n'est plus dépendant du même parent 
//       objectOnMove.parentNode.removeChild(objectOnMove);
//       event.target.appendChild(objectOnMove);


//   })
 
// }

// _____________________________________DRAG N DROP____________________________________________

  function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
}
// _____________________________________DRAG N DROP_____END____________________________________________________________________

// _____________________________________CHANGE BOX COLOR_______________________________________________________________________

function swapcolor() {
  var element = document.getElementById("boxcolor");
  element.classList.toggle("green");
}

// _____________________________________CHANGE BOX COLOR___END_________________________________________________________________

// _____________________________________CALCULS_________________________________________________________________
document.getElementById("operator", function(){

  changeOpe();
  
});

function changeOpe() {

  let ope = document.getElementById("operator");

    if (ope.innerHTML === "+") {
      ope.innerHTML = "-";
    } else {
      if (ope.innerHTML === "-") {
        ope.innerHTML = "/";
      } else {
        if (ope.innerHTML === "/") {
          ope.innerHTML = "x";
        } else {
          if (ope.innerHTML === "x") {
            ope.innerHTML = "+";
          }
        }
      }
    }

}




document.getElementById("submit", function(){

  maths();
  
});

  function maths() {
    
    
    let ope = document.getElementById("operator");
    var valuenb1 = document.getElementById("nb1").value;
    var valuenb2 = document.getElementById("nb2").value;

    let number1 = valuenb1;
    let number2 = valuenb2;
    let res = 0;
    document.getElementById("res").innerHTML =res;
    
    if (ope.innerHTML === "+") {
      res = Number (number1) + Number (number2);
      document.getElementById("res").innerHTML = res ;
    }
    if (ope.innerHTML === "-") {
      res = Number (number1) - Number (number2);
      document.getElementById("res").innerHTML = res ;
    }
    if (ope.innerHTML === "/") {
      res = Number (number1) / Number (number2);
      document.getElementById("res").innerHTML = res ;
    }
    if (ope.innerHTML === "x") {
      res = Number (number1) * Number (number2);
      document.getElementById("res").innerHTML = res ;
    }
}